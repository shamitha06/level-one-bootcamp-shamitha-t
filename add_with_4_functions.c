//Write a program to add two user input numbers using 4 functions.
	#include<stdio.h>
	float input();
	float compute(float x,float y);
	void output(float sum);
	int main()
	{
		float x,y,sum=0.0;
		printf("To add two numbers using 4 functions");
		x=input();
		y=input();
		sum=compute(x,y);
		output(sum);
		return 0;
	}
	float input()
	{
		float x;
		printf("\nEnter a number");
		scanf("%f",&x);
	}
	float compute(float x,float y)
	{
	    float sum=0.0; 
		sum=x+y;
		return sum;
	}
	void output(float sum)
	{
		printf("The sum is %f",sum);
	}

