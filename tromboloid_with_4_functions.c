//WAP to find the volume of a tromboloid using 4 functions.
	#include<stdio.h>
	float input();
	float compute(float h,float d,float b);
	void output(float vol);
	int main()
	{
		float h,d,b,vol=0.0;
		printf("To find the volume of a trombaloid using 4 functions");
		h=input();
		d=input();
		b=input();
		vol=compute(h,d,b);
		output(vol);
		return 0;
	}
	float input()
	{
		float x;
		printf("\nEnter the value");
		scanf("%f",&x);
		return x;
	}
	float compute(float h,float d,float b)
	{
		float vol=0.0;
		vol=(0.33*((h*d)+d)/b);
		return vol;
	}
	void output(float vol)
	{
		printf("\nThe volume is %f",vol);
	}
