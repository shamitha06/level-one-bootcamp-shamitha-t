//WAP to find the distance between two points using structures and 4 functions
	#include <stdio.h>
	#include <math.h>
	struct Point
	{
		float x, y;
	};
	typedef struct Point pnt ;
	float compute(pnt a, pnt b)
	{
		float dis=0.0;
		dis = sqrt(pow((a.x - b.x),2) + pow((a.y-b.y),2));
		return dis;
	}
	pnt ainput()
	{
		pnt a;
		printf("Enter coordinate of point a: ");
		scanf("%f %f", &a.x, &a.y);
		return a;
	}
	pnt binput()
	{
		pnt b;
		printf("Enter coordinate of point b: ");
		scanf("%f %f", &b.x, &b.y);
		return b;
	}
	void output(float dis)
	{
		 printf("Distance between a(%f,%f) and b(%f,%f): %f \n", a.x,a.y,b.x,b.y,dis);

	}
	int main()
	{
		pnt a,b;
		float dis=0.0;
		a=ainput();
		b=binput();
		dis=compute(a,b);
		output(dis);
		return 0;
	}

