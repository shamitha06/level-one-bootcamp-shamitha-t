//WAP to find the distance between two point using 4 functions.
	
	#include<stdio.h>
	#include<math.h>
	float x1input();
	float y1input();
	float x2input();
	float y2input();
	float compute(float x1,float x2,float y1,float y2);
	void output(float dis);
	int main()
	{
		float x1,x2,y1,y2,dis=0.0;
	    x1=x1input();
	    y1=y1input();
	    x2=x2input();
	    y2=y2input();
		dis=compute(x1,x2,y1,y2);
		output(dis);
		return 0;
	}
	float x1input()
	{
		float x1;
		printf("Enter the value of abscissa of the first point");
		scanf("%f",&x1);
		return x1;
	}
	float y1input()
	{
		float y1;
		printf("Enter the value of ordinate of the first point");
		scanf("%f",&y1);
		return y1;
	}
	float x2input()
	{
		float x2;
		printf("Enter the value of abscissa of the second point");
		scanf("%f",&x2);
		return x2;
	}
	float y2input()
	{
		float y2;
		printf("Enter the value of ordinate of the second point");
		scanf("%f",&y2);
		return y2;
	}
	float compute(float x1,float x2,float y1,float y2)
	{
		float dis=0.0;
	dis=sqrt(pow((x2-x1),2)+pow((y2-y1),2));
		return dis;
	}
	void output(float dis)
	{
		printf("The distance is %f",dis);
	}
